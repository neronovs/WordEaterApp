package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.app.Fragment;
import android.test.mock.MockContext;

import com.orhanobut.hawk.Hawk;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import okhttp3.HttpUrl;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import ru.narod.nod.wordeaterapp.data_storage.DataStorage;
import ru.narod.nod.wordeaterapp.data_storage.IDataStorage;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.network.network_provider.NetworkProvider;
import ru.narod.nod.wordeaterapp.network.retrofit.ApiFactory;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.IFragmentTranslationView;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaHooks;
import rx.schedulers.Schedulers;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(JUnit4.class)
public class FragmentTranslationPresenterImplTest {
    private FragmentTranslationPresenterImpl mPresenter;
    private IFragmentTranslationView mView;
    private List<WholeModel> practiceList;
    private NetworkProvider networkProvider;
    private IDataStorage dataStorage;
    private WholeModel lastTranslatedModel;
    private Fragment fp;
    private ApiFactory apiFactory;

    @Before
    public void setUp() throws Exception {
        mView = Mockito.mock(IFragmentTranslationView.class);
        practiceList = new ArrayList<>();

        dataStorage = new DataStorage();

        lastTranslatedModel = Mockito.mock(WholeModel.class);
        fp = Mockito.mock(Fragment.class);

        apiFactory = new ApiFactory();
//        networkProvider = Mockito.mock(NetworkProvider.class);
        networkProvider = new NetworkProvider(apiFactory);

        mPresenter = new FragmentTranslationPresenterImpl(
                mView
                , practiceList
                , networkProvider
                , dataStorage
                , lastTranslatedModel
                , fp
        );


        // Override RxJava schedulers
        RxJavaHooks.setOnIOScheduler(scheduler -> Schedulers.immediate());

        RxJavaHooks.setOnComputationScheduler(scheduler -> Schedulers.immediate());

        RxJavaHooks.setOnNewThreadScheduler(scheduler -> Schedulers.immediate());

    }

    @After
    public void tearDown() throws Exception {
        mPresenter = null;
        RxJavaHooks.reset();
        RxAndroidPlugins.getInstance().reset();
    }

    @Test
    public void getDisposables() throws Exception {
        assertNotNull(mPresenter.getDisposables());
        assertFalse(mPresenter.getDisposables().isDisposed());
        mPresenter.getDisposables().dispose();
        assertTrue(mPresenter.getDisposables().isDisposed());
    }

    @Test
    public void translate() throws Exception {
        Disposable disposable = Mockito.mock(Disposable.class);
        Map<String, String> params = new HashMap<>();
        params.put("key", "trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79");
        params.put("text", "man");
        params.put("lang", "en-ru");
        params.put("format", "plain");
        params.put("options", "1");
        mPresenter.translate(params);

        Mockito.verify(mView).showLoadingIndicator(any());

//        Mockito.verify(mView).showTranslation(new WholeModel());
    }

    @Test
    public void testServerRequest() throws Exception {
        //region init a string
        String waitingAnswer = "{\n" +
                "  \"body\": {\n" +
                "    \"headers\": {\n" +
                "      \"namesAndValues\": [\n" +
                "        \"Server\",\n" +
                "        \"nginx/1.6.2\",\n" +
                "        \"Date\",\n" +
                "        \"Thu, 01 Mar 2018 13:11:39 GMT\",\n" +
                "        \"Content-Type\",\n" +
                "        \"application/json; charset\\u003dutf-8\",\n" +
                "        \"Content-Length\",\n" +
                "        \"76\",\n" +
                "        \"Connection\",\n" +
                "        \"keep-alive\",\n" +
                "        \"Keep-Alive\",\n" +
                "        \"timeout\\u003d120\",\n" +
                "        \"Cache-Control\",\n" +
                "        \"no-store\",\n" +
                "        \"X-Content-Type-Options\",\n" +
                "        \"nosniff\"\n" +
                "      ]\n" +
                "    },\n" +
                "    \"source\": {\n" +
                "      \"buffer\": {\n" +
                "        \"size\": 0\n" +
                "      },\n" +
                "      \"closed\": false,\n" +
                "      \"source\": {\n" +
                "        \"bytesRemaining\": 76,\n" +
                "        \"closed\": false,\n" +
                "        \"timeout\": {\n" +
                "          \"deadlineNanoTime\": 0,\n" +
                "          \"hasDeadline\": false,\n" +
                "          \"timeoutNanos\": 0\n" +
                "        }\n" +
                "      }\n" +
                "    }\n" +
                "  },\n" +
                "  \"code\": 200,\n" +
                "  \"handshake\": {\n" +
                "    \"cipherSuite\": {\n" +
                "      \"javaName\": \"TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384\"\n" +
                "    },\n" +
                "    \"localCertificates\": [],\n" +
                "    \"peerCertificates\": [\n" +
                "      {\n" +
                "        \"type\": \"X.509\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\": \"X.509\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\": \"X.509\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"tlsVersion\": \"TLS_1_2\"\n" +
                "  },\n" +
                "  \"headers\": {\n" +
                "    \"namesAndValues\": [\n" +
                "      \"Server\",\n" +
                "      \"nginx/1.6.2\",\n" +
                "      \"Date\",\n" +
                "      \"Thu, 01 Mar 2018 13:11:39 GMT\",\n" +
                "      \"Content-Type\",\n" +
                "      \"application/json; charset\\u003dutf-8\",\n" +
                "      \"Content-Length\",\n" +
                "      \"76\",\n" +
                "      \"Connection\",\n" +
                "      \"keep-alive\",\n" +
                "      \"Keep-Alive\",\n" +
                "      \"timeout\\u003d120\",\n" +
                "      \"Cache-Control\",\n" +
                "      \"no-store\",\n" +
                "      \"X-Content-Type-Options\",\n" +
                "      \"nosniff\"\n" +
                "    ]\n" +
                "  },\n" +
                "  \"message\": \"OK\",\n" +
                "  \"networkResponse\": {\n" +
                "    \"code\": 200,\n" +
                "    \"handshake\": {\n" +
                "      \"cipherSuite\": {\n" +
                "        \"javaName\": \"TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384\"\n" +
                "      },\n" +
                "      \"localCertificates\": [],\n" +
                "      \"peerCertificates\": [\n" +
                "        {\n" +
                "          \"type\": \"X.509\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"type\": \"X.509\"\n" +
                "        },\n" +
                "        {\n" +
                "          \"type\": \"X.509\"\n" +
                "        }\n" +
                "      ],\n" +
                "      \"tlsVersion\": \"TLS_1_2\"\n" +
                "    },\n" +
                "    \"headers\": {\n" +
                "      \"namesAndValues\": [\n" +
                "        \"Server\",\n" +
                "        \"nginx/1.6.2\",\n" +
                "        \"Date\",\n" +
                "        \"Thu, 01 Mar 2018 13:11:39 GMT\",\n" +
                "        \"Content-Type\",\n" +
                "        \"application/json; charset\\u003dutf-8\",\n" +
                "        \"Content-Length\",\n" +
                "        \"76\",\n" +
                "        \"Connection\",\n" +
                "        \"keep-alive\",\n" +
                "        \"Keep-Alive\",\n" +
                "        \"timeout\\u003d120\",\n" +
                "        \"Cache-Control\",\n" +
                "        \"no-store\",\n" +
                "        \"X-Content-Type-Options\",\n" +
                "        \"nosniff\"\n" +
                "      ]\n" +
                "    },\n" +
                "    \"message\": \"OK\",\n" +
                "    \"protocol\": \"HTTP_1_1\",\n" +
                "    \"receivedResponseAtMillis\": 1519909924252,\n" +
                "    \"request\": {\n" +
                "      \"cacheControl\": {\n" +
                "        \"immutable\": false,\n" +
                "        \"isPrivate\": false,\n" +
                "        \"isPublic\": false,\n" +
                "        \"maxAgeSeconds\": -1,\n" +
                "        \"maxStaleSeconds\": -1,\n" +
                "        \"minFreshSeconds\": -1,\n" +
                "        \"mustRevalidate\": false,\n" +
                "        \"noCache\": false,\n" +
                "        \"noStore\": false,\n" +
                "        \"noTransform\": false,\n" +
                "        \"onlyIfCached\": false,\n" +
                "        \"sMaxAgeSeconds\": -1\n" +
                "      },\n" +
                "      \"headers\": {\n" +
                "        \"namesAndValues\": [\n" +
                "          \"Content-Length\",\n" +
                "          \"0\",\n" +
                "          \"Host\",\n" +
                "          \"translate.yandex.net\",\n" +
                "          \"Connection\",\n" +
                "          \"Keep-Alive\",\n" +
                "          \"Accept-Encoding\",\n" +
                "          \"gzip\",\n" +
                "          \"User-Agent\",\n" +
                "          \"okhttp/3.8.1\"\n" +
                "        ]\n" +
                "      },\n" +
                "      \"method\": \"POST\",\n" +
                "      \"tag\": {\n" +
                "        \"headers\": {\n" +
                "          \"namesAndValues\": []\n" +
                "        },\n" +
                "        \"method\": \"POST\",\n" +
                "        \"url\": {\n" +
                "          \"host\": \"translate.yandex.net\",\n" +
                "          \"password\": \"\",\n" +
                "          \"pathSegments\": [\n" +
                "            \"api\",\n" +
                "            \"v1.5\",\n" +
                "            \"tr.json\",\n" +
                "            \"translate\"\n" +
                "          ],\n" +
                "          \"port\": 443,\n" +
                "          \"queryNamesAndValues\": [\n" +
                "            \"options\",\n" +
                "            \"1\",\n" +
                "            \"text\",\n" +
                "            \"hi\",\n" +
                "            \"key\",\n" +
                "            \"trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\",\n" +
                "            \"format\",\n" +
                "            \"plain\",\n" +
                "            \"lang\",\n" +
                "            \"en-ru\"\n" +
                "          ],\n" +
                "          \"scheme\": \"https\",\n" +
                "          \"url\": \"https://translate.yandex.net/api/v1.5/tr.json/translate?options\\u003d1\\u0026text\\u003dhi\\u0026key\\u003dtrnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\\u0026format\\u003dplain\\u0026lang\\u003den-ru\",\n" +
                "          \"username\": \"\"\n" +
                "        }\n" +
                "      },\n" +
                "      \"url\": {\n" +
                "        \"host\": \"translate.yandex.net\",\n" +
                "        \"password\": \"\",\n" +
                "        \"pathSegments\": [\n" +
                "          \"api\",\n" +
                "          \"v1.5\",\n" +
                "          \"tr.json\",\n" +
                "          \"translate\"\n" +
                "        ],\n" +
                "        \"port\": 443,\n" +
                "        \"queryNamesAndValues\": [\n" +
                "          \"options\",\n" +
                "          \"1\",\n" +
                "          \"text\",\n" +
                "          \"hi\",\n" +
                "          \"key\",\n" +
                "          \"trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\",\n" +
                "          \"format\",\n" +
                "          \"plain\",\n" +
                "          \"lang\",\n" +
                "          \"en-ru\"\n" +
                "        ],\n" +
                "        \"scheme\": \"https\",\n" +
                "        \"url\": \"https://translate.yandex.net/api/v1.5/tr.json/translate?options\\u003d1\\u0026text\\u003dhi\\u0026key\\u003dtrnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\\u0026format\\u003dplain\\u0026lang\\u003den-ru\",\n" +
                "        \"username\": \"\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"sentRequestAtMillis\": 1519909924107\n" +
                "  },\n" +
                "  \"protocol\": \"HTTP_1_1\",\n" +
                "  \"receivedResponseAtMillis\": 1519909924252,\n" +
                "  \"request\": {\n" +
                "    \"headers\": {\n" +
                "      \"namesAndValues\": []\n" +
                "    },\n" +
                "    \"method\": \"POST\",\n" +
                "    \"tag\": {\n" +
                "      \"headers\": {\n" +
                "        \"namesAndValues\": []\n" +
                "      },\n" +
                "      \"method\": \"POST\",\n" +
                "      \"url\": {\n" +
                "        \"host\": \"translate.yandex.net\",\n" +
                "        \"password\": \"\",\n" +
                "        \"pathSegments\": [\n" +
                "          \"api\",\n" +
                "          \"v1.5\",\n" +
                "          \"tr.json\",\n" +
                "          \"translate\"\n" +
                "        ],\n" +
                "        \"port\": 443,\n" +
                "        \"queryNamesAndValues\": [\n" +
                "          \"options\",\n" +
                "          \"1\",\n" +
                "          \"text\",\n" +
                "          \"hi\",\n" +
                "          \"key\",\n" +
                "          \"trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\",\n" +
                "          \"format\",\n" +
                "          \"plain\",\n" +
                "          \"lang\",\n" +
                "          \"en-ru\"\n" +
                "        ],\n" +
                "        \"scheme\": \"https\",\n" +
                "        \"url\": \"https://translate.yandex.net/api/v1.5/tr.json/translate?options\\u003d1\\u0026text\\u003dhi\\u0026key\\u003dtrnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\\u0026format\\u003dplain\\u0026lang\\u003den-ru\",\n" +
                "        \"username\": \"\"\n" +
                "      }\n" +
                "    },\n" +
                "    \"url\": {\n" +
                "      \"host\": \"translate.yandex.net\",\n" +
                "      \"password\": \"\",\n" +
                "      \"pathSegments\": [\n" +
                "        \"api\",\n" +
                "        \"v1.5\",\n" +
                "        \"tr.json\",\n" +
                "        \"translate\"\n" +
                "      ],\n" +
                "      \"port\": 443,\n" +
                "      \"queryNamesAndValues\": [\n" +
                "        \"options\",\n" +
                "        \"1\",\n" +
                "        \"text\",\n" +
                "        \"hi\",\n" +
                "        \"key\",\n" +
                "        \"trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\",\n" +
                "        \"format\",\n" +
                "        \"plain\",\n" +
                "        \"lang\",\n" +
                "        \"en-ru\"\n" +
                "      ],\n" +
                "      \"scheme\": \"https\",\n" +
                "      \"url\": \"https://translate.yandex.net/api/v1.5/tr.json/translate?options\\u003d1\\u0026text\\u003dhi\\u0026key\\u003dtrnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79\\u0026format\\u003dplain\\u0026lang\\u003den-ru\",\n" +
                "      \"username\": \"\"\n" +
                "    }\n" +
                "  },\n" +
                "  \"sentRequestAtMillis\": 1519909924107\n" +
                "}";
        //endregion

        // Create a MockWebServer. These are lean enough that you can create a new
        // instance for every unit test.
        MockWebServer server = new MockWebServer();

        // Schedule some responses.
        server.enqueue(new MockResponse().setBody("{\"code\":200,\"detected\":{\"lang\":\"en\"},\"lang\":\"en-ru\",\"text\":[\"туман\"]}"));

        // Start the server.
        server.start();

        // Ask the server for its URL. You'll need this to make HTTP requests.
        HttpUrl baseUrl = server.url("https://translate.yandex.net/api/v1.5/tr.json/");

        // Exercise your application code, which should make those HTTP requests.
        // Responses are returned in the same order that they are enqueued.
        Map<String, String> params = new HashMap<>();
        params.put("key", "trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79");
        params.put("text", "hi");
        params.put("lang", "en-ru");
        params.put("format", "plain");
        params.put("options", "1");
        apiFactory.setBaseUrl(baseUrl);
        mPresenter.translate(params);


        // Optional: confirm that your app made the HTTP requests you were expecting.
        RecordedRequest request1 = server.takeRequest();
        assertEquals("https://translate.yandex.net/api/v1.5/tr.json/", request1.getPath());
//        assertNotNull(request1.getHeader("Authorization"));
//
//        RecordedRequest request2 = server.takeRequest();
//        assertEquals("/v1/chat/messages/2", request2.getPath());
//
//        RecordedRequest request3 = server.takeRequest();
//        assertEquals("/v1/chat/messages/3", request3.getPath());

        // Shut down the server. Instances cannot be reused.
        server.shutdown();
    }

    @Test
    public void translateOnline() throws Exception {
    }

    @Test
    public void addToPractice() throws Exception {
    }

    @Test
    public void getPracticeList() throws Exception {
    }

    @Test
    public void removeElementFromPracticeList() throws Exception {
    }

    @Test
    public void startPracticeFragment() throws Exception {
    }

}