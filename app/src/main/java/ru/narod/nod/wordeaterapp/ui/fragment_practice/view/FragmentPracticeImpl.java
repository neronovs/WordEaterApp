package ru.narod.nod.wordeaterapp.ui.fragment_practice.view;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticeListener;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticePresenter;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseViewFragment;
import ru.narod.nod.wordeaterapp.ui.progress_bar.ILoadingView;
import ru.narod.nod.wordeaterapp.ui.progress_bar.LoadingDialog;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public class FragmentPracticeImpl
        extends BaseViewFragment<IFragmentPracticePresenter>
        implements IFragmentPracticeView {

    //region Variable declaration
    @BindView(R.id.practice_fragment_et_source)
    TextView tv_source;

    @BindView(R.id.practice_et_frag)
    View et_trans_frag;

    @BindView(R.id.practice_fragment_btn_check)
    Button btn_check;

    @BindView(R.id.practice_fragment_btn_back)
    Button btn_back;

    @BindView(R.id.practice_frag_imageView)
    ImageView practice_frag_imageView;

    @Inject
    IFragmentPracticeListener listener;

    @Inject
    List<WholeModel> practiceList;

    private final String TAG = getClass().getName();
    private ILoadingView mILoadingView;

    private EditText editText;
    //endregion

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.getDisposables().clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_practice,
                container, false);

        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        //Declare a progress bar field
        mILoadingView = LoadingDialog.view(getFragmentManager());

        editText = et_trans_frag.findViewById(R.id.et_fragmentet_edittext);
        et_trans_frag.findViewById(R.id.et_fragment_btn_go).setOnClickListener(v ->
                listener.checkTranslation(editText.getText().toString()));
        editText.setHint(getResources().getString(R.string.eng_hint_enter_translation));

        presenter.initMakingMatch();

    }

    //Click handler
    @OnClick({R.id.practice_fragment_btn_check})
    public void check_buttonClick() {
        listener.checkTranslation(editText.getText().toString());
    }
    @OnClick({R.id.practice_fragment_btn_back})
    public void back_buttonClick() {
        getActivity().onBackPressed();
    }


    @Override
    public void makeMatch(WholeModel model) {
        //Log.i(TAG, " makeMatch(). Request id is:  " + model.getRequest_text());
        tv_source.setText(model.getRequest_text());
    }

    @Override
    //Putting translated id to appropriate EditText
    public void showSuccess(String text) {
//        Log.i(TAG, " showSuccess(). Text is:  " + model.getResponse_text().getList(0));
        Toast.makeText(getActivity().getApplicationContext(),
                text, Toast.LENGTH_SHORT).show();

        editText.setText("");
    }

    @Override
    public void putPicture(String urlOfPicture) {
        if (urlOfPicture == null) {
            Picasso.with(getActivity())
                    .load(R.drawable.image_no)
                    .into(practice_frag_imageView);
        } else {
            Picasso.with(getActivity())
                    .load(urlOfPicture)
                    .placeholder(R.drawable.image_loading)
                    .error(R.drawable.image_no)
                    .into(practice_frag_imageView);
        }

        Log.i(TAG, " putPicture(). urlOfPicture is:  " + urlOfPicture);

    }

}