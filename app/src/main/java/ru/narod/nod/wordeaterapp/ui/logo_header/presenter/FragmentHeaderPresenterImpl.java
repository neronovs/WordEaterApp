package ru.narod.nod.wordeaterapp.ui.logo_header.presenter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.data_storage.IDataStorage;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.network.network_provider.INetworkCreator;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationListener;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationPresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.IFragmentTranslationView;
import ru.narod.nod.wordeaterapp.ui.general.presenter.BasePresenter;
import ru.narod.nod.wordeaterapp.ui.logo_header.view.FragmentHeaderImpl;
import ru.narod.nod.wordeaterapp.ui.logo_header.view.IFragmentHeaderView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@PerFragment
final public class FragmentHeaderPresenterImpl
        extends BasePresenter<IFragmentHeaderView>
        implements IFragmentHeaderPresenter {

    @Inject
    FragmentHeaderPresenterImpl(@NonNull IFragmentHeaderView view) {
        super(view);
    }

}
