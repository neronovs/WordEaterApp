package ru.narod.nod.wordeaterapp;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

/**
 * Injects application dependencies.
 */
@Singleton
@Component(modules = AppModule.class)
interface IAppComponent
        extends AndroidInjector<App> {

    @Component.Builder
    abstract class Builder
            extends AndroidInjector.Builder<App> {
    }

}