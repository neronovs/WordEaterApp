package ru.narod.nod.wordeaterapp.ui.main_activity;

import android.app.Fragment;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import dagger.android.HasFragmentInjector;
import io.fabric.sdk.android.Fabric;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.general.BaseActivity;
import ru.narod.nod.wordeaterapp.ui.logo_header.view.FragmentHeaderImpl;

//WordEater App
public class MainActivity
        extends BaseActivity
        implements HasFragmentInjector {

    @BindView(R.id.fragm_container)
    RelativeLayout container;

    @BindView(R.id.header_container)
    ConstraintLayout header_container;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Inject
    List<WholeModel> practiceList;
    private Fragment mFragmentTranslationImpl;
    private Fragment mFragmentHeaderImpl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.activity_main);
        //Init ButterKnife
        ButterKnife.bind(this);

        //Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        mFragmentTranslationImpl = new FragmentTranslationImpl();
        mFragmentHeaderImpl = new FragmentHeaderImpl();

        if (savedInstanceState == null) {
            //Adding the header
            addFragment(R.id.header_container, mFragmentHeaderImpl);

            //Setting the provideTranslationObserver fragment on the main activity
            addFragment(R.id.fragm_container, mFragmentTranslationImpl);
        }
    }

}
