package ru.narod.nod.wordeaterapp.ui.edit_text.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.network.NetworkUtils;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventBtnGo;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventEditText;
import ru.narod.nod.wordeaterapp.ui.edit_text.presenter.IFragmentEditTextPresenter;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseViewFragment;

public class FragmentEditTextImpl
        extends BaseViewFragment<IFragmentEditTextPresenter>
        implements IFragmentEditTextView {

    private static final int MIN_TEXT_LENGTH = 1;
    private static final String EMPTY_STRING = "";

    private final String TRANSLATION_FORMAT = "plain";
    private final String TRANSLATION_OPTION = "1";

    private final String TAG = getClass().getName();
    private String hint;

    @BindView(R.id.et_fragmentet_textinputlayout)
    TextInputLayout mTextInputLayout;

    @BindView(R.id.et_fragmentet_edittext)
    EditText mEditText;

    @BindView(R.id.et_fragment_btn_go)
    Button mBtnGo;

    @BindView(R.id.et_fragment_btn_clean)
    Button mBtnClean;

    @Inject
    EventBus bus;

    //region Setters/Getters
    public EditText getEditText() {
        return mEditText;
    }

    public void setEditText_string(String text) {
        mEditText.setText(text);
        hideErrorEditText();
    }

    public void setHint(String text) {
        hint = text;
        if (mTextInputLayout != null)
            mTextInputLayout.setHint(text);
    }
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edittext,
                container, false);

        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        mTextInputLayout.setHint(hint);
//        mEditText.setOnEditorActionListener(ActionListener.newInstance(this));

    }

    @OnFocusChange(R.id.et_fragmentet_edittext)
    void etClicked(EditText editText) {
        if (editText.isFocused())
            bus.post(new MessageEventEditText(editText.hashCode()));
    }

    @OnClick(R.id.et_fragment_btn_go)
    void btnGoClicked() {
        Log.i(TAG, "It was pressed R.id.et_fragment_btn_go");

        if (shouldShowError()) {
            showErrorEditText();
            showError(new Throwable(getResources().getString(R.string.eng_enter_what_translate)));
        } else {
            if (NetworkUtils.isOnline(getActivity())) {
                hideErrorEditText();
                makeParamsAndPost();
            }
        }
    }

    //Preparing and posting info through the Event Bus
    public Map<String, String> makeParamsAndPost() {
        Map<String, String> params = new HashMap<>();
        params.put("text", mEditText.getText().toString());
        params.put("lang", getTag());
        params.put("format", TRANSLATION_FORMAT);
        params.put("options", TRANSLATION_OPTION);
        //Sending parametres through the bus event to the FragmentTranslationImpl to start REST API
        bus.post(new MessageEventBtnGo(params));

        return params;
    }


    @OnClick(R.id.et_fragment_btn_clean)
    void btnCleanoClicked() {
        Log.i(TAG, "It was pressed R.id.et_fragment_btn_clean");

        cleanTextField(mEditText);
    }

    private void cleanTextField(EditText mEditText) {
        mEditText.setText("");
    }


    public boolean shouldShowError() {
        int textLength = mEditText.getText().length();
        return textLength < MIN_TEXT_LENGTH;
    }

    public void showErrorEditText() {
        mTextInputLayout.setError(getString(R.string.eng_enter_what_translate));
    }

    public void hideErrorEditText() {
        mTextInputLayout.setError(EMPTY_STRING);
    }


    private static final class ActionListener implements TextView.OnEditorActionListener {
        private final WeakReference<FragmentEditTextImpl> textInputLayout_EditText_wr;

        public static ActionListener newInstance(FragmentEditTextImpl inputLayout_editText) {
            WeakReference<FragmentEditTextImpl> textInputLayout_EditText_wr = new WeakReference<>(inputLayout_editText);
            return new ActionListener(textInputLayout_EditText_wr);
        }

        private ActionListener(WeakReference<FragmentEditTextImpl> textInputLayout_EditText_wr) {
            this.textInputLayout_EditText_wr = textInputLayout_EditText_wr;
        }

        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            FragmentEditTextImpl fragmentTranslation = textInputLayout_EditText_wr.get();
            if (fragmentTranslation != null) {
                if (fragmentTranslation.shouldShowError()) {
                    fragmentTranslation.showErrorEditText();
                } else {
                    fragmentTranslation.hideErrorEditText();
                }
            }
            return true;
        }
    }

}
