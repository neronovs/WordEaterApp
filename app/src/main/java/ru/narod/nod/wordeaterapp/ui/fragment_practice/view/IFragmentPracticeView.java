package ru.narod.nod.wordeaterapp.ui.fragment_practice.view;

import io.reactivex.disposables.Disposable;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;
import ru.narod.nod.wordeaterapp.ui.general.view.MVPView;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IFragmentPracticeView
        extends MVPView {

    void makeMatch(WholeModel model);

    void showSuccess(String text);

    void showError(Throwable throwable);

    void putPicture(String urlOfPicture);

    void hideLoadingIndicator();

    void showLoadingIndicator();

    void showLoadingIndicator(Disposable disposable);
}
