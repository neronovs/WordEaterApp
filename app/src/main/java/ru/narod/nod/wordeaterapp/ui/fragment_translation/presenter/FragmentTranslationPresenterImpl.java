package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.app.FragmentManager;
import android.support.annotation.NonNull;
import android.app.Fragment;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.xml.parsers.ParserConfigurationException;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.data_storage.IDataStorage;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.network.network_provider.INetworkCreator;
import ru.narod.nod.wordeaterapp.network.network_provider.NetworkProvider;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.IFragmentTranslationView;
import ru.narod.nod.wordeaterapp.ui.general.presenter.BasePresenter;

@PerFragment
final public class FragmentTranslationPresenterImpl
        extends BasePresenter<IFragmentTranslationView>
        implements IFragmentTranslationPresenter, IFragmentTranslationListener {

    //region Variable declaration
//    private final TranslationUseCase mUseCase;
    private static final String API_KEY = "trnsl.1.1.20171123T102837Z.66d69d8a92e951db.753d34b2db6d8740a2157fb1507a7132c137da79";

    private static String API_ENDPOINT_URL_TRANSL = "https://translate.yandex.net/api/v1.5/tr.json/";
    public void setApiEndpointUrlTransl(String apiEndpointUrlTransl) {
        API_ENDPOINT_URL_TRANSL = apiEndpointUrlTransl;
    }

    //    private final String TAG = getClass().getName();
    private final String DB_KEY_PRACTICE = "practice";
    private final String DB_KEY_LANG = "lang_spinner";

    private final CompositeDisposable disposables = new CompositeDisposable();

//    private List<WholeModel> practiceList;

    //    @Inject
    private List<WholeModel> practiceList;
    //    @Inject
    private NetworkProvider networkProvider;
    //    @Inject
    private IDataStorage dataStorage;
    //    @Inject
    private WholeModel lastTranslatedModel;
    //    @Inject
    private Fragment fp;

    //!!!!!!!!!!!!!!! For testing answer from server
//    String serverAnswer = "";

    //endregion

    @Inject
    FragmentTranslationPresenterImpl(
            @NonNull IFragmentTranslationView view
            , List<WholeModel> practiceList
            , NetworkProvider networkProvider
            , IDataStorage dataStorage
            , WholeModel lastTranslatedModel
            , Fragment fp
    ) {
        super(view);
        this.practiceList = practiceList;
        this.networkProvider = networkProvider;
        this.dataStorage = dataStorage;
        this.lastTranslatedModel = lastTranslatedModel;
        this.fp = fp;
    }

    //Getters and setters
    public CompositeDisposable getDisposables() {
        return disposables;
    }

    //Creating params and send them to UseCase to initialise API request to provideTranslationObserver service
    @Override
    public void translate(Map<String, String> params) {

        params.put("key", API_KEY);

        disposables.add( //To destroy calls due to closing a fragment
                networkProvider.getTranslationRepository()
                        .provideTranslationObserver(params, API_ENDPOINT_URL_TRANSL)
                        .subscribeOn(io.reactivex.schedulers.Schedulers.io())
                        .doOnSubscribe(view::showLoadingIndicator)
                        .doAfterTerminate(view::hideLoadingIndicator)
                        .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe(model -> {
                            fillModelWithRequestInfo(model, params);
                            lastTranslatedModel = model;
                            view.showTranslation(model);
                        }, view::showError)
        );

        Log.i("TAG", "btnGoLongClick() finished");
    }

    @Override
    public io.reactivex.Observable<WholeModel> translateOnline(Map<String, String> params) {
        params.put("key", API_KEY);

        io.reactivex.Observable observable = networkProvider.getTranslationRepository()
                .provideTranslationObserver(params, API_ENDPOINT_URL_TRANSL)
                .subscribeOn(io.reactivex.schedulers.Schedulers.io())
                .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                .map(model -> {
                    fillModelWithRequestInfo(model, params);
                    lastTranslatedModel = model;
                    return model;
                });

        return observable;
    }


    //Filling Model with request information before returning
    private void fillModelWithRequestInfo(WholeModel model, Map<String, String> params) {
        model.setRequest_key(params.get("key"));
        model.setRequest_text(params.get("text"));
        model.setRequest_lang(params.get("lang"));
        model.setRequest_format(params.get("format"));
        model.setRequest_options(params.get("options"));
    }

    //Saving a words pair to train using the DB
    @Override
    public void addToPractice(String et_first, String et_second, WholeModel lastTranslatedModelToPractice) {
//        Log.i(TAG, "addToPractice() has started!");
        if (dataStorage.contains(DB_KEY_PRACTICE)) {
            practiceList = dataStorage.getList(DB_KEY_PRACTICE);
            dataStorage.delete(DB_KEY_PRACTICE);
        } else {
            practiceList = new ArrayList<>();
        }

        disposables.add( //To destroy calls due to closing a fragment
                io.reactivex.Observable.just(practiceList)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(list -> {
                            if (lastTranslatedModelToPractice.getRequest_lang().substring(0, 2).equals("ru")) {
                                lastTranslatedModelToPractice.setRequest_text(et_second.trim());
                                lastTranslatedModelToPractice.setResponse_text(et_first.trim());
                            } else {
                                lastTranslatedModelToPractice.setRequest_text(et_first.trim());
                                lastTranslatedModelToPractice.setResponse_text(et_second.trim());
                            }
                            list.add(lastTranslatedModelToPractice);

                            dataStorage.putList(list, DB_KEY_PRACTICE);
                            return list;
                        })
                        .subscribe(view::fillPracticeList)
        );

//            Log.i(TAG, "addToPractice() HAWK transaction is done!");
    }

    //Getting a words pair to train
    @Override
    public void getPracticeList() {
//        Log.i(TAG, "getPracticeList() has started!");

        //Recovering of the saved practice list from DB with HAWK lib
        disposables.add( //To destroy calls due to closing a fragment
                Observable.just(practiceList)
                        .subscribeOn(Schedulers.io())
                        .map(list -> {
                            if (dataStorage.contains(DB_KEY_PRACTICE)) {
                                practiceList = list = dataStorage.getList(DB_KEY_PRACTICE);
                            }
                            return list;
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(view::fillPracticeList)
        );

//        Log.i(TAG, "getPracticeList() HAWK transaction is done!");
    }

    //Remove a word pair from DB
    @Override
    public void removeElementFromPracticeList(int position) {
//        Log.i(TAG, "removeElementFromPracticeList() has started!");
        List<WholeModel> models = new ArrayList<>();

        disposables.add( //To destroy calls due to closing a fragment
                Observable.just(models)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(list -> {
                            dataStorage.removeFromList(position, DB_KEY_PRACTICE);
                            return (List) list;
                        })
//                .subscribe(fragmentView::fillPracticeList);
                        .subscribe()
        );

//        Log.i(TAG, "removeElementFromPracticeList() HAWK transaction is done!");
    }

    @Override
    public void startPracticeFragment(FragmentManager supportFragmentManager) {
//        Fragment fp = new FragmentPracticeImpl();
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragm_container, fp, "practiceFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public int getCurrentLangSpinnerPosition() {
        return dataStorage.loadInt(DB_KEY_LANG);
    }

    @Override
    public void saveSelectedLangSpinnerPositionToDB(int position) {
        dataStorage.saveInt(DB_KEY_LANG, position);
    }
}