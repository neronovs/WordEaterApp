package ru.narod.nod.wordeaterapp.ui.fragment_details.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.fragment_details.presenter.FragmentDetailsPresenterModule;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.FragmentPracticePresenterModule;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentDetailsPresenterModule.class
})
public abstract class FragmentDetailsModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentDetails the practice fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentDetailsImpl fragmentDetails);

    @Binds
    @PerFragment
    abstract IFragmentDetailsView fragmentDetailsView(FragmentDetailsImpl fragmentDetails);

}