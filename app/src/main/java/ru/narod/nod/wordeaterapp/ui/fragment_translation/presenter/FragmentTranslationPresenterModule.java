package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.app.Fragment;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentTranslationPresenterModule {

    @Binds
    @PerFragment
    abstract IFragmentTranslationPresenter fragmentTranslationPresenter (
            FragmentTranslationPresenterImpl fragmentTranslationPresenterImpl);

    /**
     * The presenter listens to the events in the {@link FragmentTranslationImpl}.
     *
     * @param fragmentTranslationPresenterImpl the presenter
     * @return the FragmentTranslationImpl listener
     */
    @Binds
    @PerFragment
    abstract IFragmentTranslationListener fragmentTranslationListener(
            FragmentTranslationPresenterImpl fragmentTranslationPresenterImpl);

    @Provides
    @PerFragment
    static WholeModel currentWholeModel() {
        return new WholeModel();
    }

    @Provides
    @PerFragment
    static Fragment fragmentPractice() {
        return new FragmentPracticeImpl();
    }

}