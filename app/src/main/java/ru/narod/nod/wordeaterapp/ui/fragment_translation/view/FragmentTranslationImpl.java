package ru.narod.nod.wordeaterapp.ui.fragment_translation.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.network.NetworkUtils;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventBtnGo;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventEditText;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventRowClicked;
import ru.narod.nod.wordeaterapp.ui.edit_text.presenter.RxSearchObservable;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.IFragmentEditTextView;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.IFragmentDetailsView;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationListener;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationPresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.RecyclerRowAdapter;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseViewFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public class FragmentTranslationImpl
        extends BaseViewFragment<IFragmentTranslationPresenter>
        implements IFragmentTranslationView {

    //region Variable declaration
    private final String TAG = getClass().getName();
    private final String CONTAINER_FIRST_POSITION_EN = "en-ru";
    private final String CONTAINER_SECOND_POSITION_EN = "ru-en";
    private final String CONTAINER_FIRST_POSITION_DE = "de-ru";
    private final String CONTAINER_SECOND_POSITION_DE = "ru-de";
    private final String DETAILS_TAG = "details tag";

    private RecyclerRowAdapter mDataAdapter;
    private List<WholeModel> practiceList;

    private WholeModel lastTranslatedModel;

    @BindView(R.id.main_fragment_btn_add)
    Button btn_add;
    @BindView(R.id.main_fragment_btn_practice)
    Button btn_practice;
    @BindView(R.id.main_fragmentet_rv)
    RecyclerView rv;
    @BindView(R.id.main_fragment_cont_1)
    LinearLayout cont_1;
    @BindView(R.id.main_fragment_spinner_lang)
    Spinner spinner_lang;

    @Inject
    IFragmentTranslationListener listener;
    @Inject
    EventBus bus;
    @Inject
    IFragmentEditTextView editText_first;
    @Inject
    IFragmentEditTextView editText_second;
    @Inject
    IFragmentDetailsView fragmentDetailsView;
    @Inject
    RxSearchObservable rxSearchObservable;
    //endregion

    //Treatment for cards swiping. Using to remove cards from the practice list
    ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(
            0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT
//            | ItemTouchHelper.DOWN | ItemTouchHelper.UP
    ) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            Toast.makeText(activityContext, "on Move", Toast.LENGTH_SHORT).show();
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
//            Toast.makeText(getContext(), "on Swiped ", Toast.LENGTH_SHORT).show();

            //Remove swiped item from list and notify the RecyclerView
            int position = viewHolder.getAdapterPosition();
            presenter.removeElementFromPracticeList(position);
            mDataAdapter.remove(position);
            mDataAdapter.notifyDataSetChanged();
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translation,
                container, false);

        if (!bus.isRegistered(this))
            bus.register(this);

        return view;
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        practiceList = new ArrayList<>();

        //Recycler view creating
        mDataAdapter = new RecyclerRowAdapter(practiceList, bus);
        rv.setLayoutManager(new GridLayoutManager(activityContext, 1,
                GridLayoutManager.VERTICAL, false));
        rv.setAdapter(mDataAdapter);

        //Creating handler
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rv);

        //Loading earlier saved the practice list
        presenter.getPracticeList();

        //Set fragment EditText for the cont_1 container if the container is empty
        if (cont_1.getChildCount() == 0) {
            editText_first.setHint(getResources().getString(R.string.eng_hint_enter_english_word));
            addChildFragment(R.id.main_fragment_cont_1, (Fragment) editText_first, CONTAINER_FIRST_POSITION_EN);
            //Set another fragment EditText for the cont_1 container
            editText_second.setHint(getResources().getString(R.string.eng_hint_enter_russian_word));
            addChildFragment(R.id.main_fragment_cont_1, (Fragment) editText_second, CONTAINER_SECOND_POSITION_EN);
        }

        prepareLangSpinner();

    }

    //Prepare the lang spinner
    private void prepareLangSpinner() {
        String[] langsArray = getResources().getStringArray(R.array.lang_list);
        //Making an ArrayAdapter with the Resources array and standard layout of the Spinner element
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, langsArray);
        //Init a layout for choosing using of an element
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Set an adapter for the view
        spinner_lang.setAdapter(adapter);
        spinner_lang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listener.saveSelectedLangSpinnerPositionToDB(position);
                setLanguageForHintFields();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        spinner_lang.setSelection(listener.getCurrentLangSpinnerPosition());

        setLanguageForHintFields();
    }

    //Set lang to fields
    private void setLanguageForHintFields() {
        if (spinner_lang.getSelectedItem().toString().equals(CONTAINER_FIRST_POSITION_EN))
            editText_first.setHint(getResources().getString(R.string.eng_hint_enter_english_word));
        if (spinner_lang.getSelectedItem().toString().equals(CONTAINER_FIRST_POSITION_DE))
            editText_first.setHint(getResources().getString(R.string.eng_hint_enter_deutsch_word));
    }

    //Prepare translation params
    private Map<String, String> changeParams(Map<String, String> params) {
        if (spinner_lang.getSelectedItem().toString().equals("de-ru"))
            if (params.get("lang").equals(CONTAINER_FIRST_POSITION_EN)) {
                params.put("lang", "de-ru");
            } else if (params.get("lang").equals(CONTAINER_SECOND_POSITION_EN)) {
                params.put("lang", "ru-de");
            }

        return params;
    }

    //Using to receive info from an EditText fragment
    //Bus subscription for translation button pressing
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEventBtnGo event) {
        listener.translate(changeParams(event.params));
    }

    //Subscription for online translation handling
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEventEditText event) {
//        Log.i(TAG, "MessageEventEditText: " + event);
        setUpSearchObservableForOnlineTranslation(editText_first.getEditText(), editText_second.getEditText());
    }

    //Subscription for row clicking treatment in the recycler view
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEventRowClicked event) {
        fragmentDetailsView.setWholeModel(event.getWholeModel());
        replaceParentFragment(R.id.fragm_container, (Fragment) fragmentDetailsView, DETAILS_TAG);
    }



    //region OnClick handler methods
    @OnClick(R.id.main_fragment_btn_add)
    void onAddToPracticeClicked() {
        Log.i(TAG, "It was pressed R.id.main_fragment_btn_add");
        if (!editText_first.getEditText().getText().toString().isEmpty()
                && !editText_second.getEditText().getText().toString().isEmpty()) {
            listener.addToPractice(editText_first.getEditText().getText().toString(),
                    editText_second.getEditText().getText().toString(),
                    lastTranslatedModel);
        } else {
            showError(new Throwable(activityContext.getResources()
                    .getString(R.string.eng_fill_both_fields)));
        }
    }

    @OnClick(R.id.main_fragment_btn_practice)
    void onStartPracticeFragmentClicked() {
        Log.i(TAG, "It was pressed R.id.main_fragment_btn_practice");
        if (rv.getChildCount() > 0)
            listener.startPracticeFragment(getActivity().getFragmentManager());
        else
            showError(new Throwable(getResources().getString(R.string.eng_practice_list_empty)));
    }
    //endregion

    @Override
    //The method that is calling from presenter after getting translated id
    public void fillPracticeList(List<WholeModel> practiceList) {
        this.practiceList = practiceList;

        //Starting a refresh list in the Adapter
        if (practiceList != null && !practiceList.isEmpty()) {
            mDataAdapter.add(practiceList);
            mDataAdapter.notifyDataSetChanged(); //.changeDataSet(practiceList);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.getDisposables().clear();
    }

    private void setUpSearchObservableForOnlineTranslation(EditText etFirst, EditText etSecond) {
        if (NetworkUtils.isOnline(getActivity())) {

            presenter.getDisposables().add( //Add the call to a disposable to kill it if a fragment closed
                    rxSearchObservable.fromView(etFirst)
                    .debounce(700, TimeUnit.MILLISECONDS)
                    .filter(text -> !text.isEmpty())
                    .distinctUntilChanged()
                    .switchMap(query -> listener.translateOnline(editText_first.makeParamsAndPost())
                            .map(wholeModel -> lastTranslatedModel = wholeModel)
                            .map(wholeModel -> wholeModel.getResponse_text().get(0)))
                    .subscribeOn(Schedulers.io())
                    .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                    .subscribe(etSecond::setText, this::showError)
            );
        }
    }


    @Override
    public void showLoadingIndicator(Disposable disposable) {
        super.showLoadingIndicator();
    }

    @Override
    //Putting translated id to appropriate EditText
    public void showTranslation(@NonNull WholeModel model) {
        Log.i(TAG, " showSuccess(). Text is:  " + model.getResponse_text().get(0));

        lastTranslatedModel = model;

        //Unsubscribe when use a translate button to prevent reverse translation
        rxSearchObservable.unsubscribe();

        if (model.getResponse_lang().equals(CONTAINER_SECOND_POSITION_EN) ||
                model.getResponse_lang().equals(CONTAINER_SECOND_POSITION_DE)) {
            editText_first.setEditText_string(model.getResponse_text().get(0));
        } else {
            editText_second.setEditText_string(model.getResponse_text().get(0));
        }

        //We need again subscribe because we unsubscribe when use a translate button to prevent reverse translation
        rxSearchObservable.subscribe();
    }

}