package ru.narod.nod.wordeaterapp.ui.edit_text.view;

import android.widget.EditText;

import java.util.Map;

import ru.narod.nod.wordeaterapp.ui.general.view.MVPView;

public interface IFragmentEditTextView
        extends MVPView {

    boolean shouldShowError();

    void showError(Throwable throwable);

    void hideErrorEditText();

    void setHint(String text);

    EditText getEditText();

    void setEditText_string(String text);

    Map<String, String> makeParamsAndPost();

}


