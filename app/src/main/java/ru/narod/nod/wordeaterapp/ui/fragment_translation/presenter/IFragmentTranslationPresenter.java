package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.app.FragmentManager;

import io.reactivex.disposables.CompositeDisposable;
import ru.narod.nod.wordeaterapp.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentTranslationPresenter extends Presenter {

    void getPracticeList();

    void removeElementFromPracticeList(int position);

    CompositeDisposable getDisposables();

    void setApiEndpointUrlTransl(String apiEndpointUrlTransl);

}
