package ru.narod.nod.wordeaterapp.ui.edit_text.presenter;

import android.view.View;

import dagger.Provides;
import ru.narod.nod.wordeaterapp.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentEditTextPresenter
        extends Presenter {

    void clickList(View v);

}
