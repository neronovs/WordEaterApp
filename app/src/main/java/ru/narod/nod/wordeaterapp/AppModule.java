package ru.narod.nod.wordeaterapp;


import android.app.Application;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjectionModule;
import dagger.android.ContributesAndroidInjector;
import ru.narod.nod.wordeaterapp.data_storage.DataStorage;
import ru.narod.nod.wordeaterapp.data_storage.IDataStorage;
import ru.narod.nod.wordeaterapp.inject.PerActivity;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.network.network_provider.NetworkProvider;
import ru.narod.nod.wordeaterapp.network.retrofit.ApiFactory;
import ru.narod.nod.wordeaterapp.network.retrofit.NetworkModule;
import ru.narod.nod.wordeaterapp.ui.main_activity.MainActivity;
import ru.narod.nod.wordeaterapp.ui.main_activity.MainActivityModule;

@Module(includes = { AndroidInjectionModule.class, NetworkModule.class})
abstract class AppModule {

    @Binds
    @Singleton
    /*
     * Singleton annotation isn't necessary since Application instance is unique but is here for
     * convention. In general, providing Activity, Fragment, BroadcastReceiver, etc does not require
     * them to be scoped since they are the components being injected and their instance is unique.
     *
     * However, having a scope annotation makes the module easier to read. We wouldn't have to look
     * at what is being provided in order to understand its scope.
     */
    abstract Application application(App app);

    /**
     * Provides the injector for the {@link MainActivity}, which has access to the dependencies
     * provided by this application instance (singleton scoped objects).
     */
    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity mainActivityInjector();

    @Binds
    @Singleton
    abstract IDataStorage iDataStorage(DataStorage dataStorage);

//    @Provides
//    @Singleton
//    static IDataStorage dataStorage() {
//        return new DataStorage();
//    }

    @Provides
    @Singleton
    static NetworkProvider networkProvider() {
        return new NetworkProvider(new ApiFactory());
    }

    @Provides
    @Singleton //Cannot be abstract because can't @Provides for ArrayList
    public static List<WholeModel> practiceList() {
        return new ArrayList<>();
    }

    @Provides
    @Singleton
    public static EventBus getBus() {
        return EventBus.getDefault();
    }

}