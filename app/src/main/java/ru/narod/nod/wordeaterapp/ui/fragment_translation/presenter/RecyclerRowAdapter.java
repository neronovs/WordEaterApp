package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventBtnGo;
import ru.narod.nod.wordeaterapp.rx_bus.MessageEventRowClicked;

public class RecyclerRowAdapter
        extends RecyclerView.Adapter<RecyclerRowAdapter.ItemViewHolder> {

    private List<WholeModel> wholeModels;

    private EventBus bus;

    public RecyclerRowAdapter(List<WholeModel> wholeModels, EventBus bus) {
        this.wholeModels = wholeModels;
        this.bus = bus;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.fragment_translation_recycler_row, parent, false);

        return new ItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.requestText.setText(wholeModels.get(position).getRequest_text());
        holder.responseText.setText(wholeModels.get(position).getResponse_text().get(0));

        holder.itemView.setOnClickListener(v -> onRecyclerRowSelected(position));
    }

    private void onRecyclerRowSelected(int index) {
        Log.i(getClass().getName(), "onRecyclerRowSelected() was called!");

        bus.post(new MessageEventRowClicked(wholeModels.get(index)));
//        mEventBus.send(BusEvents.SELECT_PHONE.setData(index));
    }

    @Override
    public int getItemCount() {
        return wholeModels.size();
    }

    public void remove(int position) {
        wholeModels.remove(position);

    }

    public void add(List<WholeModel> wholeModels) {
        this.wholeModels = wholeModels;
    }

    //This View will be used to display the item inside recylerview
    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView requestText;
        TextView responseText;
//        LinearLayout ll;

        public ItemViewHolder(View v) {
            super(v);
            requestText = v.findViewById(R.id.rec_row_rus_words);
            responseText = v.findViewById(R.id.rec_row_eng_words);
//            ll = v.findViewById(R.id.rec_row_ll);

        }
    }

}