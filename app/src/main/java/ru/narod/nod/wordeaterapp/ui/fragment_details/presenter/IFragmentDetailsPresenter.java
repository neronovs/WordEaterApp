package ru.narod.nod.wordeaterapp.ui.fragment_details.presenter;

import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentDetailsPresenter extends Presenter {

    void getWikiArticle();

    CompositeDisposable getDisposables();

    void setCurrentWholeModel(WholeModel currentWholeModel);

}
