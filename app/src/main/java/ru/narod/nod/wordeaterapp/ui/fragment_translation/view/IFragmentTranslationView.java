package ru.narod.nod.wordeaterapp.ui.fragment_translation.view;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.disposables.Disposable;
import ru.narod.nod.wordeaterapp.ui.general.view.MVPView;
import ru.narod.nod.wordeaterapp.ui.progress_bar.ILoadingView;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IFragmentTranslationView
        extends ILoadingView, MVPView {

    void showTranslation(@NonNull WholeModel model);

    void showError(Throwable throwable);

    void fillPracticeList(List<WholeModel> practiceList);

    void showLoadingIndicator(Disposable disposable);
}


