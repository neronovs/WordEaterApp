package ru.narod.nod.wordeaterapp.network.retrofit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import ru.narod.nod.wordeaterapp.inject.PerActivity;

@Module
public class NetworkModule {

    @Provides
    @PerActivity
    ApiFactory apiFactory() {
        return new ApiFactory();
    }

    @Provides
    @PerActivity
    OkHttpClient.Builder okHttpClient() {
        return new OkHttpClient.Builder();
    }

    @Provides
    @PerActivity
    HttpLoggingInterceptor httpLoggingInterceptor() {
        return new HttpLoggingInterceptor();
    }

}
