package ru.narod.nod.wordeaterapp.rx_bus;

import java.util.Map;

public class MessageEventBtnGo {
 
    public final Map<String, String> params;
 
    public MessageEventBtnGo(Map<String, String> params) {
        this.params = params;
    }
}