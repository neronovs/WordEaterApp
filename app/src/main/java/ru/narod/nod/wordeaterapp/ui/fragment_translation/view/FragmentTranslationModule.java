package ru.narod.nod.wordeaterapp.ui.fragment_translation.view;

import android.app.Fragment;

import net.kibotu.android.recyclerviewpresenter.PresenterAdapter;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.edit_text.presenter.RxSearchObservable;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextImpl;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextModule;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.IFragmentEditTextView;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.FragmentDetailsImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.IFragmentDetailsView;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.FragmentTranslationPresenterModule;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentTranslationPresenterModule.class
})
public abstract class FragmentTranslationModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentTranslationImpl the provideTranslationObserver fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentTranslationImpl fragmentTranslationImpl);

    @Binds
    @PerFragment
    abstract IFragmentTranslationView fragmentTranslationView(FragmentTranslationImpl fragmentTranslationImpl);

    @Provides
    @PerFragment
    static PresenterAdapter<WholeModel> dataAdapter() {
        return new PresenterAdapter<>();
    }

    @Provides
//    @PerFragment
    static IFragmentEditTextView fragmentEditText() {
        return new FragmentEditTextImpl();
    }

    @Provides
    @PerFragment
    static RxSearchObservable rxSearchObservable() {
        return new RxSearchObservable();
    }

    @Provides
    @PerFragment
    static IFragmentDetailsView fragmentDetailsView() {
        return new FragmentDetailsImpl();
    }
}