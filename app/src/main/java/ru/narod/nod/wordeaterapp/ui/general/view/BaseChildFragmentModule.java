package ru.narod.nod.wordeaterapp.ui.general.view;

import dagger.Module;

@Module
public abstract class BaseChildFragmentModule {
  
    public static final String CHILD_FRAGMENT = "BaseChildFragmentModule.childFragment";
}