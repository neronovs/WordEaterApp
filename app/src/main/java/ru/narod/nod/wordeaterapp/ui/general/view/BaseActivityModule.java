package ru.narod.nod.wordeaterapp.ui.general.view;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import javax.inject.Named;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ru.narod.nod.wordeaterapp.inject.PerActivity;

/**
 * Provides base activity dependencies. This must be included in all activity modules, which must
 * provide a concrete implementation of {@link Activity}.
 */
@Module
public abstract class BaseActivityModule {

    public static final String ACTIVITY_FRAGMENT_MANAGER = "BaseActivityModule.activityFragmentManager";

    @Binds
    @PerActivity
    abstract Context activityContext(Activity activity);

    @Provides
    @Named(ACTIVITY_FRAGMENT_MANAGER)
    @PerActivity
    static FragmentManager activityFragmentManager(Activity activity) {
        return activity.getFragmentManager();
    }
}