package ru.narod.nod.wordeaterapp.ui.edit_text.presenter;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import javax.inject.Inject;

import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.IFragmentEditTextView;
import ru.narod.nod.wordeaterapp.ui.general.presenter.BasePresenter;

@PerFragment
final public class FragmentEditTextPresenterImpl
        extends BasePresenter<IFragmentEditTextView>
        implements IFragmentEditTextPresenter {

    private final String TAG = getClass().getName();

    @Inject
    FragmentEditTextPresenterImpl(@NonNull IFragmentEditTextView view) {
        super(view);
    }

    @Override
    public void clickList(View v) {
        Log.i(TAG, " clickList(). It was pressed view: " + v);

    }

}
