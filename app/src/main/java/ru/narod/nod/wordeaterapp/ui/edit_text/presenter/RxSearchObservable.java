package ru.narod.nod.wordeaterapp.ui.edit_text.presenter;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.TextView;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxSearchObservable {
    private static final PublishSubject<String> subject = PublishSubject.create();
    private static boolean flagSubscription = true;

    public Observable<String> fromView(EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (flagSubscription)
                    subject.onNext(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
//                subject.onComplete();
            }
        });
//        {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                subject.onComplete();
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String id) {
//                subject.onNext(id);
//                return true;
//            }
//        });

        return subject;
    }

    public void unsubscribe() {
        flagSubscription = false;
    }
    public void subscribe() {
        flagSubscription = true;
    }
}