package ru.narod.nod.wordeaterapp.ui.logo_header.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationPresenter;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseViewFragment;
import ru.narod.nod.wordeaterapp.ui.logo_header.presenter.IFragmentHeaderPresenter;

public class FragmentHeaderImpl
        extends BaseViewFragment<IFragmentHeaderPresenter>
        implements IFragmentHeaderView {

    @BindView(R.id.main_logo_header)
    ImageView main_logo_header;

//    @Inject
    public FragmentHeaderImpl() {}

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.logo_header,
                container, false);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @OnClick(R.id.main_logo_header)
    public void headerClick() {
        YoYo.with(Techniques.Pulse)
                .duration(200)
                .repeat(0)
                .playOn(main_logo_header);
    }

}
