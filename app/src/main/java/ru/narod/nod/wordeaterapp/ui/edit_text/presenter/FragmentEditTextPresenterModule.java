package ru.narod.nod.wordeaterapp.ui.edit_text.presenter;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentEditTextPresenterModule {

    @Binds
    @PerFragment
    abstract IFragmentEditTextPresenter fragmentEditTextPresenter(
            FragmentEditTextPresenterImpl fragmentEditTextPresenterImpl);



}