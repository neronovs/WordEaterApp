package ru.narod.nod.wordeaterapp.network.network_provider;

import javax.inject.Inject;

import ru.narod.nod.wordeaterapp.network.retrofit.ApiFactory;
import ru.narod.nod.wordeaterapp.ui.progress_bar.ILoadingView;

public class NetworkProvider {

    private static INetworkCreator sTranslationRepository;
    private static INetworkCreator sPictureRepository;
    private static INetworkCreator sWikiRepository;

//    @Inject
    private ApiFactory apiFactory;

    @Inject
    public NetworkProvider(ApiFactory apiFactory) {
        this.apiFactory = apiFactory;
    }

    public INetworkCreator getTranslationRepository() {
        if (sTranslationRepository == null) {
            sTranslationRepository = new NetworkCreatorImpl(apiFactory);
        }
        return sTranslationRepository;
    }

    public INetworkCreator getPictureRepository() {
        if (sPictureRepository == null) {
            sPictureRepository = new NetworkCreatorImpl(apiFactory);
        }
        return sPictureRepository;
    }

    public INetworkCreator getWikiRepository() {
        if (sWikiRepository == null) {
            sWikiRepository = new NetworkCreatorImpl(apiFactory);
        }
        return sWikiRepository;
    }

}
