package ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter;

import android.app.FragmentManager;

import io.reactivex.disposables.CompositeDisposable;
import ru.narod.nod.wordeaterapp.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentPracticePresenter extends Presenter {

    void initMakingMatch();

    CompositeDisposable getDisposables();

}
