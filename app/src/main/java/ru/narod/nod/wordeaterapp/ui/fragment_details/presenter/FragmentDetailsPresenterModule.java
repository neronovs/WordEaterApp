package ru.narod.nod.wordeaterapp.ui.fragment_details.presenter;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.FragmentPracticePresenterImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticeListener;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticePresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentDetailsPresenterModule {

    @Binds
    @PerFragment
    abstract IFragmentDetailsPresenter fragmentDetailsPresenter(
            FragmentDetailsPresenterImpl fragmentDetailsPresenterImpl);

}