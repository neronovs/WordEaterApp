package ru.narod.nod.wordeaterapp.ui.fragment_practice.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.FragmentPracticePresenterModule;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentPracticePresenterModule.class
})
public abstract class FragmentPracticeModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentPracticeImpl the practice fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentPracticeImpl fragmentPracticeImpl);

    @Binds
    @PerFragment
    abstract IFragmentPracticeView fragmentPracticeView(FragmentPracticeImpl fragmentPracticeImpl);
}