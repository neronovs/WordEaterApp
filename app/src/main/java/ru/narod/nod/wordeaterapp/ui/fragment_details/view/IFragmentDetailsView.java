package ru.narod.nod.wordeaterapp.ui.fragment_details.view;

import io.reactivex.disposables.Disposable;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.general.view.MVPView;

public interface IFragmentDetailsView
        extends MVPView {

    void showWiki(String text);

    void showError(Throwable throwable);

    void hideLoadingIndicator();

    void showLoadingIndicator();

    void showLoadingIndicator(Disposable disposable);

    void setWholeModel(WholeModel wholeModel);
}
