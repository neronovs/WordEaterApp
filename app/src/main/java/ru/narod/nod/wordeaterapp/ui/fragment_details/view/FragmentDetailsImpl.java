package ru.narod.nod.wordeaterapp.ui.fragment_details.view;

import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import ru.narod.nod.wordeaterapp.R;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.fragment_details.presenter.IFragmentDetailsPresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticeListener;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticePresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseViewFragment;
import ru.narod.nod.wordeaterapp.ui.progress_bar.ILoadingView;
import ru.narod.nod.wordeaterapp.ui.progress_bar.LoadingDialog;

public class FragmentDetailsImpl
        extends BaseViewFragment<IFragmentDetailsPresenter>
        implements IFragmentDetailsView {

    //region Variable declaration
    private final String TAG = getClass().getName();
    private ILoadingView mILoadingView;

    @BindView(R.id.details_fragment_tv_description)
    TextView textView;

    private static WholeModel wholeModel;

    @Inject
    public FragmentDetailsImpl() {}

    @Override
    public void showWiki(String text) {
        Log.i(TAG, " showWiki(). ResponseBody is:  " + text);

        textView.setText(text);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }

    public void setWholeModel(WholeModel wholeModel) {
        FragmentDetailsImpl.wholeModel = wholeModel;
    }
    //endregion

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.getDisposables().clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details,
                container, false);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        //Declare a progress bar field
        mILoadingView = LoadingDialog.view(getFragmentManager());

        presenter.setCurrentWholeModel(wholeModel);
        presenter.getWikiArticle();
    }

    //Click handler
    @OnClick({R.id.details_fragment_btn_back})
    public void back_buttonClick() {
        getActivity().onBackPressed();
    }

}