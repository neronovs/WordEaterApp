package ru.narod.nod.wordeaterapp.network.retrofit;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IWikiService {

    @GET("api.php")
    Observable<ResponseBody> wikiService(
            @QueryMap Map<String, String> params
    );

}
