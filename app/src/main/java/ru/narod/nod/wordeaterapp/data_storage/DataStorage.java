package ru.narod.nod.wordeaterapp.data_storage;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.narod.nod.wordeaterapp.model.WholeModel;

public class DataStorage
        implements IDataStorage {

    @Inject
    public DataStorage() {}

    //region List handling
    @Override
    public void putList(List<WholeModel> models, String key) {
        Hawk.put(key, models);
    }

    @Override
    public void addInList(WholeModel model, String containKey) {
        List<WholeModel> models;

        if (contains(containKey)) {
            models = Hawk.get(containKey);
            delete(containKey);
        } else {
            models = new ArrayList<>();
        }

        models.add(model);
        putList(models, containKey);
    }

    @Override
    public void removeFromList(int position, String containKey) {
        List<WholeModel> models;

        if (contains(containKey)) {
            models = getList(containKey);

            if (models.get(position) != null)
                models.remove(position);

            delete(containKey);
        } else {
            models = new ArrayList<>();
        }

        putList(models, containKey);
    }

    @Override
    public List<WholeModel> getList(final String KEY) {
        List<WholeModel> models;

        if (contains(KEY)) {
            models = Hawk.get(KEY);

            return models;
        } else {
            models = new ArrayList<>();
        }

        return models;
    }
    //endregion

    //region String handling
    @Override
    public void saveString(String key, String str) {
        if (contains(key)) {
            delete(key);
        }

        Hawk.put(key, str);
    }

    @Override
    public String loadString(String key) {
        String str = "";
        if (contains(key)) {
            str = Hawk.get(key);
        }

        return str;
    }
    //endregion

    //region int handling
    @Override
    public void saveInt(String key, int int_val) {
        if (contains(key)) {
            delete(key);
        }

        Hawk.put(key, int_val);
    }

    @Override
    public int loadInt(String key) {
        int int_val;
        if (contains(key)) {
            int_val = Hawk.get(key);
        } else {
            int_val = 0;
        }

        return int_val;
    }
    //endregion

    @Override
    public boolean contains(String key) {
        return Hawk.contains(key);
    }

    @Override
    public void delete(String key) {
        Hawk.delete(key);
    }
}