package ru.narod.nod.wordeaterapp.model;

import android.annotation.SuppressLint;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class WholeModel {

    //region Image url to use with Flickr
//    private String picUrl = "";
//    public String getPicUrl() {
//        return picUrl;
//    }
//    public void setPicUrl(String picUrl) {
//        this.picUrl = picUrl;
//    }
    //endregion

    @SuppressLint("UseSparseArrays")
    private Map<Integer, String> codesMap = new HashMap<>();

    @Inject
    public WholeModel() {
        codesMap.put(200, "Операция выполнена успешно");
        codesMap.put(401, "Неправильный API-ключ");
        codesMap.put(402, "API-ключ заблокирован");
        codesMap.put(404, "Превышено суточное ограничение на объем переведенного текста");
        codesMap.put(413, "Превышен максимально допустимый размер текста");
        codesMap.put(422, "Текст не может быть переведен");
        codesMap.put(501, "Заданное направление перевода не поддерживается");
    }

    //region Request part
    //The has been using to getList provideTranslationObserver from API service
    @SerializedName("key")
    @Expose
    private String request_key;

    //The translating text
    @SerializedName("request_text")
    @Expose
    private String request_text;

    //A way of provideTranslationObserver: en-ru, ru-en
    @SerializedName("request_lang")
    @Expose
    private String request_lang;

    //Two variants: plain — simple text, html — HTML formatted text
    @SerializedName("request_format")
    @Expose
    private String request_format;

    //Only one variant - "1". If the parameter is using, the API service will determine what the source text language is
    @SerializedName("request_options")
    @Expose
    private String request_options;

    public String getRequest_key() {
        return request_key;
    }

    public void setRequest_key(String request_key) {
        this.request_key = request_key;
    }

    public String getRequest_text() {
        return request_text;
    }

    public void setRequest_text(String request_text) {
        this.request_text = request_text;
    }

    public String getRequest_lang() {
        return request_lang;
    }

    public void setRequest_lang(String request_lang) {
        this.request_lang = request_lang;
    }

    public String getRequest_format() {
        return request_format;
    }

    public void setRequest_format(String request_format) {
        this.request_format = request_format;
    }

    public String getRequest_options() {
        return request_options;
    }

    public void setRequest_options(String request_options) {
        this.request_options = request_options;
    }
    //endregion



    //region Response part
    /***
     * 200 Операция выполнена успешно
     * 401 Неправильный API-ключ
     * 402 API-ключ заблокирован
     * 404 Превышено суточное ограничение на объем переведенного текста
     * 413 Превышен максимально допустимый размер текста
     * 422 Текст не может быть переведен
     * 501 Заданное направление перевода не поддерживается
     */
    private enum TranslationCodes {
//        TRANSLATION_SUCCESSFUL(200, "Операция выполнена успешно"),
//        KEY_WRONG(401, "Неправильный API-ключ"),
//        KEY_BLOCKED(402, "API-ключ заблокирован"),
//        OVER_TRANSLATION_PER_DAY(404, "Превышено суточное ограничение на объем переведенного текста"),
//        OVER_TEXT_SIZE(413, "Превышен максимально допустимый размер текста"),
//        TEXT_UNTRASLATABLE(422, "Текст не может быть переведен"),
//        UNSUPPORTED_TRASLATION_WAY(501, "Заданное направление перевода не поддерживается")
//        ;
//
//        private String codeName;
//        private int codeNum;
//        TranslationCodes(int codeNum, String codeName) {
//        }
//
//        public String getCodeName(int code) {
//            return codeName;
//        }
    }
    @SerializedName("code")
    @Expose
    private Integer response_code;

    //A way of provideTranslationObserver: en-ru, ru-en
    @SerializedName("lang")
    @Expose
    private String response_lang;

    //Translating response_text
    @SerializedName("text")
    @Expose
    private List<String> response_text = null;

    public Integer getResponse_code() {
        return response_code;
    }
    //Get information from CODE that received from the translation service
    public String getResponse_code_info() {
        return response_code!=null?codesMap.get(response_code):"";
    }

    public void setResponse_code(Integer response_code) {
        this.response_code = response_code;
    }

    public String getResponse_lang() {
        return response_lang;
    }

    public void setResponse_lang(String response_lang) {
        this.response_lang = response_lang;
    }

    public List<String> getResponse_text() {
        return response_text;
    }

    public void setResponse_text(String response_text) {
        this.response_text.clear();
        this.response_text.add(response_text);
    }
    //endregion


    //Picture handling
//    @SerializedName("owner")
//    @Expose
//    private String owner;
//
//    @SerializedName("id")
//    @Expose
//    private String id;



//    @SerializedName("engVal")
//    @Expose
//    private String engVal;
//
//    public String getRusVal() {
//        return rusVal;
//    }
//
//    public void setRusVal(String rusVal) {
//        this.rusVal = rusVal;
//    }
//
//    public String getEngVal() {
//        return engVal;
//    }
//
//    public void setEngVal(String engVal) {
//        this.engVal = engVal;
//    }
}