package ru.narod.nod.wordeaterapp.ui.progress_bar;


public interface ILoadingView {

    void hideLoadingIndicator();

    void showLoadingIndicator();
}
