package ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter;

import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IFragmentPracticeListener {

    void checkTranslation(String translatedText);

}