package ru.narod.nod.wordeaterapp.network.retrofit;

import java.lang.annotation.Retention;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Qualifier;

import dagger.Provides;
import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import ru.narod.nod.wordeaterapp.model.WholeModel;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

public interface ITranslateService {

    @POST("translate")
    Observable<WholeModel> translateService(
            @QueryMap Map<String, String> params
    );

    // for RxJava v.2
    @POST("translate")
    Observable<WholeModel> translateServiceRX2(
            @QueryMap Map<String, String> params
    );

}
