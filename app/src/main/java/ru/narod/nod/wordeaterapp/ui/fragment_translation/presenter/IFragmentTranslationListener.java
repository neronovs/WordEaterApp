package ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter;

import android.app.FragmentManager;

import java.util.Map;

import io.reactivex.Observable;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IFragmentTranslationListener {


    void translate(Map<String, String> params);
    Observable<WholeModel> translateOnline(Map<String, String> params);

    void addToPractice(String et_russian, String et_english, WholeModel wholeModel);

    void startPracticeFragment(FragmentManager supportFragmentManager);

    int getCurrentLangSpinnerPosition();
    void saveSelectedLangSpinnerPositionToDB(int position);

}