package ru.narod.nod.wordeaterapp.network.retrofit;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;

public interface IPictureService {

    @POST(".")
    Observable<FlickrPicturesModel> pictureService(
            @QueryMap Map<String, String> params
    );

}
