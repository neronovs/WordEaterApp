package ru.narod.nod.wordeaterapp.rx_bus;

import ru.narod.nod.wordeaterapp.model.WholeModel;

public class MessageEventRowClicked {

    public WholeModel getWholeModel() {
        return wholeModel;
    }

    private WholeModel wholeModel;

    public MessageEventRowClicked(WholeModel wholeModel) {
        this.wholeModel = wholeModel;
    }
}