package ru.narod.nod.wordeaterapp.network.retrofit;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    //region Variable declaration
    private static String API_ENDPOINT_URL = null;
    private static final String API_ENDPOINT_URL_TRANSL = "https://translate.yandex.net/api/v1.5/tr.json/";
    private static final String API_ENDPOINT_URL_PICTURE = "https://api.flickr.com/services/rest/";
    private static final String API_ENDPOINT_URL_WIKI = "wikipedia.org/w/";

    private static OkHttpClient sClient;
    private static ITranslateService sTransService;
    private static IPictureService sPicService;
    private static IWikiService sWikiService;

    private HttpUrl baseUrl;

    public void setBaseUrl(HttpUrl baseUrl) {
        this.baseUrl = baseUrl;
    }
    //endregion

    @Inject
    public ApiFactory() {
    }

    //Creating and returning API service
    public Object getService(String API_ENDPOINT_URL) {
        //I know that double checked locking is not a good pattern, but it's enough here
        ApiFactory.API_ENDPOINT_URL = API_ENDPOINT_URL;
        Object service = chooseService(API_ENDPOINT_URL);
        if (service == null) {
            synchronized (ApiFactory.class) {
                switch (API_ENDPOINT_URL) {
                    case API_ENDPOINT_URL_TRANSL:
                        service = sTransService;
                        if (service == null) {
                            service = sTransService = (ITranslateService) createService();
                        }
                        break;
                    case API_ENDPOINT_URL_PICTURE:
                        service = sPicService;
                        if (service == null) {
                            service = sPicService = (IPictureService) createService();
                        }
                        break;
//                    case API_ENDPOINT_URL_WIKI:
//                        service = sWikiService;
//                        if (service == null) {
//                            service = sWikiService = (IWikiService) createService();
//                        }
//                        break;
                }
            }
        }
        if (API_ENDPOINT_URL.substring(11).equals(API_ENDPOINT_URL_WIKI)) {
            service = sWikiService = (IWikiService) createService();
        }

        return service;
    }

    private Object chooseService(String api_endpoint_url) {
        if (api_endpoint_url.equals(API_ENDPOINT_URL_TRANSL))
            return sTransService;
        else if (api_endpoint_url.equals(API_ENDPOINT_URL_PICTURE))
            return sPicService;
        else if (api_endpoint_url.substring(11).equals(API_ENDPOINT_URL_WIKI))
            return sWikiService;
        else
            return null;
    }
    //Creating and returning API service
//    public IPictureService getPictureService(String API_ENDPOINT_URL) {
//        //I know that double checked locking is not a good pattern, but it's enough here
//        ApiFactory.API_ENDPOINT_URL = API_ENDPOINT_URL;
//        IPictureService service = sPicService;
//        if (service == null) {
//            synchronized (ApiFactory.class) {
//                service = sPicService;
//                if (service == null) {
//                    service = sPicService = createPicService();
//                }
//            }
//        }
//        return service;
//    }


    //Init an interface for the Retrofit lib
    private Object createService() {
        baseUrl = HttpUrl.parse(API_ENDPOINT_URL);
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())     // for RxJava v.2
                .build();
        Object service;
        if (API_ENDPOINT_URL.equals(API_ENDPOINT_URL_TRANSL))
            service = retrofit.create(ITranslateService.class);
        else if (API_ENDPOINT_URL.equals(API_ENDPOINT_URL_PICTURE))
            service = retrofit.create(IPictureService.class);
        else
            service = retrofit.create(IWikiService.class);

        return service;
    }
    //Init an interface for the Retrofit lib
//    private IPictureService createPicService() {
//        Gson gson = new GsonBuilder()
//                .setLenient()
//                .create();
//        return new Retrofit.Builder()
//                .baseUrl(API_ENDPOINT_URL)
//                .client(getClient())
//                .addConverterFactory(GsonConverterFactory.create(gson))
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
////                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
//                .build()
//                .create(IPictureService.class);
//    }


    //Making OkHttp instance
    private OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }


    private OkHttpClient.Builder okHttpClient;
    private HttpLoggingInterceptor logging;

    //Using to putList Interceptors
    private OkHttpClient buildClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message ->
                Log.i("OkHttp_Logger", message));
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        return new OkHttpClient.Builder()
                .addInterceptor(logging)
                .addInterceptor(new ApiKeyInterceptor())
                .build();
    }

}
