package ru.narod.nod.wordeaterapp.ui.logo_header.view;

import android.support.annotation.NonNull;

import java.util.List;

import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.general.view.MVPView;
import ru.narod.nod.wordeaterapp.ui.progress_bar.ILoadingView;

public interface IFragmentHeaderView
        extends ILoadingView, MVPView {


}


