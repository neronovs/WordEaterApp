package ru.narod.nod.wordeaterapp.ui.logo_header.presenter;

import ru.narod.nod.wordeaterapp.ui.general.presenter.Presenter;

/**
 * A {@link Presenter} that does some work and shows the results.
 */
public interface IFragmentHeaderPresenter extends Presenter {

}
