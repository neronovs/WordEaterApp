package ru.narod.nod.wordeaterapp.ui.logo_header.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.FragmentTranslationPresenterModule;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.IFragmentTranslationView;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseFragmentModule;
import ru.narod.nod.wordeaterapp.ui.logo_header.presenter.FragmentHeaderPresenterModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentHeaderPresenterModule.class
})
public abstract class FragmentHeaderModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragmentHeader the provideTranslationObserver fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentHeaderImpl fragmentHeader);

    @Binds
    @PerFragment
    abstract IFragmentHeaderView fragmentHeaderView(FragmentHeaderImpl fragmentHeader);

}