package ru.narod.nod.wordeaterapp.network.network_provider;

import java.util.Map;

import okhttp3.ResponseBody;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;
import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface INetworkCreator {

    io.reactivex.Observable<WholeModel> provideTranslationObserver(
            Map<String, String> params, final String API_ENDPOINT_URL);

    io.reactivex.Observable<ResponseBody> provideWikiObserver(
            Map<String, String> params, final String API_ENDPOINT_URL);

    io.reactivex.Observable<FlickrPicturesModel> providePictureObserver(
            Map<String, String> params, final String API_ENDPOINT_URL);

}
