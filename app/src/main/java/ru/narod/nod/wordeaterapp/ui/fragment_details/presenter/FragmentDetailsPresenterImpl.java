package ru.narod.nod.wordeaterapp.ui.fragment_details.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import ru.narod.nod.wordeaterapp.data_storage.DataStorage;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;
import ru.narod.nod.wordeaterapp.network.network_provider.NetworkProvider;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.IFragmentDetailsView;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticeListener;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter.IFragmentPracticePresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.general.presenter.BasePresenter;

@PerFragment
public class FragmentDetailsPresenterImpl
        extends BasePresenter<IFragmentDetailsView>
        implements IFragmentDetailsPresenter {

    //region Variable declaration
    private static WholeModel currentWholeModel;
    public void setCurrentWholeModel(WholeModel currentWholeModel) {
        FragmentDetailsPresenterImpl.currentWholeModel = currentWholeModel;
    }

    private static final String API_ENDPOINT_URL_WIKI_1 = "https://";
    private static final String API_ENDPOINT_URL_WIKI_2 = ".wikipedia.org/w/";
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    NetworkProvider networkProvider;
    //endregion

    @Inject
    FragmentDetailsPresenterImpl(@NonNull IFragmentDetailsView view) {
        super(view);
    }

    @Override
    public void getWikiArticle() {
        Map<String, String> params = new HashMap<>();
        params.put("action", "opensearch");
        params.put("search", currentWholeModel.getRequest_text());
//        params.put("limit", "1");
        params.put("format", "json");

        String API_ENDPOINT_URL_WIKI = API_ENDPOINT_URL_WIKI_1 +
                currentWholeModel.getRequest_lang().substring(0, 2) + API_ENDPOINT_URL_WIKI_2;

        disposables.add( //To destroy calls due to closing a fragment
                networkProvider.getWikiRepository()
                        .provideWikiObserver(params, API_ENDPOINT_URL_WIKI)
                        .subscribeOn(io.reactivex.schedulers.Schedulers.io())
                        .observeOn(io.reactivex.android.schedulers.AndroidSchedulers.mainThread())
                        .doOnSubscribe(view::showLoadingIndicator)
                        .doAfterTerminate(view::hideLoadingIndicator)
                        .map(this::parseRawResponse)
                        .subscribe(view::showWiki
                                , view::showError)
        );

        Log.i("TAG", "btnGoLongClick() finished");
    }

    private String parseRawResponse(ResponseBody responseBody) {
        StringBuilder textToTextView = new StringBuilder();
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(responseBody.string());
            JSONArray jsonArray2 = (JSONArray) jsonArray.get(2);

            for (int i = 0; i < jsonArray2.length(); i++) {
                textToTextView.append(" • ").append(jsonArray2.get(i)).append("\n");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textToTextView.toString();
    }

    //Getters and setters
    public CompositeDisposable getDisposables() {
        return disposables;
    }

}
