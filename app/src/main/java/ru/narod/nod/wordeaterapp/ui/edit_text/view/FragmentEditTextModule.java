package ru.narod.nod.wordeaterapp.ui.edit_text.view;

import android.app.Fragment;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.edit_text.presenter.FragmentEditTextPresenterModule;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseFragmentModule;

/**
 * Provides example 1 fragment dependencies.
 */
@Module(includes = {
        BaseFragmentModule.class,
        FragmentEditTextPresenterModule.class
})
public abstract class FragmentEditTextModule {

    /**
     * As per the contract specified in {@link BaseFragmentModule}; "This must be included in all
     * fragment modules, which must provide a concrete implementation of {@link Fragment}
     * and named {@link BaseFragmentModule#FRAGMENT}.
     *
     * @param fragment_editTextImpl the provideTranslationObserver fragment
     * @return the fragment
     */
    @Binds
    @Named(BaseFragmentModule.FRAGMENT)
    @PerFragment
    abstract Fragment fragment(FragmentEditTextImpl fragment_editTextImpl);

    @Binds
    @PerFragment
    abstract IFragmentEditTextView textInputLayout_EditText_view(FragmentEditTextImpl fragment_editTextImpl);

//    /**
//     * The presenter listens to the events in the {@link FragmentTranslationImpl}.
//     *
//     * @param fragmentTranslationPresenterImpl the presenter
//     * @return the FragmentTranslationImpl listener
//     */
//    @Binds
//    @PerFragment
//    abstract IFragmentTranslationListener fragmentTranslationListener(
//            FragmentTranslationPresenterImpl fragmentTranslationPresenterImpl);
}