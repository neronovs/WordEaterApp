package ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter;

import android.support.annotation.NonNull;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ru.narod.nod.wordeaterapp.data_storage.DataStorage;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;
import ru.narod.nod.wordeaterapp.network.network_provider.NetworkProvider;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.IFragmentPracticeView;
import ru.narod.nod.wordeaterapp.ui.general.presenter.BasePresenter;

@PerFragment
public class FragmentPracticePresenterImpl
        extends BasePresenter<IFragmentPracticeView>
        implements IFragmentPracticePresenter, IFragmentPracticeListener {

    //region Variable declaration
    private static WholeModel currentWholeModel;
    private List<WholeModel> practiceList;
    private final String FLICKR_API_KEY = "37c48974e6976e5e480c83eef3cfc8ed";
    private static final String API_ENDPOINT_URL_PICTURE = "https://api.flickr.com/services/rest/";
    private final CompositeDisposable disposables = new CompositeDisposable();

    @Inject
    NetworkProvider networkProvider;
    //endregion

    @Inject
    FragmentPracticePresenterImpl(@NonNull IFragmentPracticeView view,
                                  @NonNull List<WholeModel> practiceList,
                                  @NonNull WholeModel currentWholeModel,
                                  @NonNull DataStorage dataStorage
    ) {
        super(view);
        this.practiceList = practiceList;
        FragmentPracticePresenterImpl.currentWholeModel = currentWholeModel;
        final String DB_KEY_PRACTICE = "practice";

        if (dataStorage.contains(DB_KEY_PRACTICE)) {
            practiceList.addAll(dataStorage.getList(DB_KEY_PRACTICE));
        }
    }

    //Getters and setters
    public CompositeDisposable getDisposables() {
        return disposables;
    }

    private WholeModel shuffledWholeModelFromPracticeList() {
        Collections.shuffle(practiceList);

        currentWholeModel = practiceList.get(0);

        return currentWholeModel;
    }

    @Override
    public void initMakingMatch() {
        view.makeMatch(shuffledWholeModelFromPracticeList());

        //The pattern is "https://api.flickr.com/services/rest/?&api_key=4ef2fe2affcdd6e13218f5ddd0e2500d&format=json&method=flickr.photos.search&text=carton"
        Map<String, String> params = new HashMap<>();
        params.put("api_key", FLICKR_API_KEY);
        params.put("format", "json");
        params.put("method", "flickr.photos.search");
        params.put("text", currentWholeModel.getRequest_text());
        params.put("nojsoncallback", "1"); //To change JSONP to JSON https://stackoverflow.com/questions/15930932/how-do-i-parse-this-flickr-response

        disposables.add( //To destroy calls due to closing a fragment
                networkProvider.getPictureRepository()
                .providePictureObserver(params, API_ENDPOINT_URL_PICTURE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(view::showLoadingIndicator)
                .doAfterTerminate(view::hideLoadingIndicator)
                .subscribe(model -> {
                    String url = null;
                    int listSize = model.getPhotos().getPhoto().size();
                            if (listSize > 0) url = pictureUrlMaking(model);
                            view.putPicture(url);
                        }
                        , view::showError)
        );

    }

    private String pictureUrlMaking(FlickrPicturesModel model) {
        final String URL_SIZE_SUFFIX = "_m_d";
        final String URL_EXT_SUFFIX = ".jpg";

        int index = makeRandomInt(model.getPhotos().getPhoto().size());

        String url = "https://farm4.staticflickr.com/"
                + model.getPhotos().getPhoto().get(index).getServer() + "/"
                + model.getPhotos().getPhoto().get(index).getId() + "_"
                + model.getPhotos().getPhoto().get(index).getSecret()
                + URL_SIZE_SUFFIX + URL_EXT_SUFFIX
                ;

//        currentWholeModel.setPicUrl(url);

        return url;
    }
    //Random generator for integer number
    private int makeRandomInt(int maxNumberOfInt) {
        return new Random().nextInt(maxNumberOfInt);
    }

    @Override
    public void checkTranslation(String translatedText) {
        translatedText = textTreater(translatedText);
        if (currentWholeModel != null) {
            if (textTreater(translatedText).
                    equals(textTreater(currentWholeModel.getResponse_text().get(0)))) {
                String text = "Yes, \"" + currentWholeModel.getRequest_text() +
                        "\" translated as \"" + translatedText + "\"";
                view.showSuccess(text);

                initMakingMatch();

            } else {
                String text = "Oops, \"" + currentWholeModel.getRequest_text() +
                        "\" actually is \"" + currentWholeModel.getResponse_text().get(0) + "\"";
                view.showSuccess(text);
            }
        }
    }

    //Makes id without spaces in the beginning and the end and makes id as lowcase
    private String textTreater(String inputText) {
        return inputText.trim().toLowerCase();
    }

}
