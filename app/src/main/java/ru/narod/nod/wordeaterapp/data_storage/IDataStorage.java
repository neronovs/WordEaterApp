package ru.narod.nod.wordeaterapp.data_storage;

import java.util.List;

import ru.narod.nod.wordeaterapp.model.WholeModel;

public interface IDataStorage {

    void putList(List<WholeModel> models, String key);
    void addInList(WholeModel model, String containKey);
    void removeFromList(int position, String containKey);
    List<WholeModel> getList(String key);


    void saveString(String str, String key);
    String loadString(String key);


    void saveInt(String key, int int_val);
    int loadInt(String key);


    boolean contains(String key);
    void delete(String key);

}
