package ru.narod.nod.wordeaterapp.ui.fragment_practice.presenter;

import dagger.Binds;
import dagger.Module;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentPracticePresenterModule {

    @Binds
    @PerFragment
    abstract IFragmentPracticePresenter fragmentPracticePresenter(
            FragmentPracticePresenterImpl fragmentPracticePresenterImpl);

    /**
     * The presenter listens to the events in the {@link FragmentPracticeImpl}.
     *
     * @param fragmentPracticePresenterImpl the presenter
     * @return the FragmentTranslationImpl listener
     */
    @Binds
    @PerFragment
    abstract IFragmentPracticeListener fragmentPracticeListener(
            FragmentPracticePresenterImpl fragmentPracticePresenterImpl);

}