package ru.narod.nod.wordeaterapp.ui.main_activity;

import android.app.Activity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.narod.nod.wordeaterapp.inject.PerActivity;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextImpl;
import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextModule;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.FragmentDetailsImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_details.view.FragmentDetailsModule;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_practice.view.FragmentPracticeModule;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationModule;
import ru.narod.nod.wordeaterapp.ui.general.view.BaseActivityModule;
import ru.narod.nod.wordeaterapp.ui.logo_header.view.FragmentHeaderImpl;
import ru.narod.nod.wordeaterapp.ui.logo_header.view.FragmentHeaderModule;

/**
 * Provides main activity dependencies.
 */
@Module(includes = BaseActivityModule.class)
public abstract class MainActivityModule {

    /**
     * Provides the injector for the {@link FragmentDetailsImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentDetailsModule.class)
    abstract FragmentDetailsImpl fragmentDetailsInjector();

    /**
     * Provides the injector for the {@link FragmentEditTextImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentEditTextModule.class)
    abstract FragmentEditTextImpl textInputLayoutInjector();

    /**
     * Provides the injector for the {@link FragmentTranslationImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentTranslationModule.class)
    abstract FragmentTranslationImpl fragmentTranslationInjector();


    /**
     * Provides the injector for the {@link FragmentPracticeImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentPracticeModule.class)
    abstract FragmentPracticeImpl fragmentPracticeInjector();

    /**
     * Provides the injector for the {@link FragmentHeaderImpl}, which has access to the dependencies
     * provided by this activity and application instance (singleton scoped objects).
     */
    @PerFragment
    @ContributesAndroidInjector(modules = FragmentHeaderModule.class)
    abstract FragmentHeaderImpl fragmentHeader();

    /**
     * As per the contract specified in {@link BaseActivityModule}; "This must be included in all
     * activity modules, which must provide a concrete implementation of {@link Activity}."
     * <p>
     * This provides the activity required to inject the
     * {@link BaseActivityModule#ACTIVITY_FRAGMENT_MANAGER} into the
     * {@link ru.narod.nod.wordeaterapp.ui.general.BaseActivity}.
     *
     * @param mainActivity the activity
     * @return the activity
     */
    @Binds
    @PerActivity
    abstract Activity activity(MainActivity mainActivity);

}
