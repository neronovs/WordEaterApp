package ru.narod.nod.wordeaterapp.network.retrofit;

import android.support.annotation.NonNull;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.narod.nod.wordeaterapp.data_storage.DataStorage;
import ru.narod.nod.wordeaterapp.data_storage.IDataStorage;

public class ApiKeyInterceptor implements Interceptor {

    private final String LAST_JSON_RESPONSE_KEY = "last_response";
    private IDataStorage dataStorage;

    @Override
    //Using for testing
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl url = request.url().newBuilder()
                .build();
        request = request.newBuilder().url(url)
                .build();

        Response response = chain.proceed(request);

        dataStorage = new DataStorage();
        dataStorage.saveString(LAST_JSON_RESPONSE_KEY, new GsonBuilder().setPrettyPrinting().create().toJson(response));

        return response;
    }
}