package ru.narod.nod.wordeaterapp.network.network_provider;

import android.util.Log;

import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import ru.narod.nod.wordeaterapp.model.flickr.FlickrPicturesModel;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.network.retrofit.ApiFactory;
import ru.narod.nod.wordeaterapp.network.retrofit.IPictureService;
import ru.narod.nod.wordeaterapp.network.retrofit.ITranslateService;
import ru.narod.nod.wordeaterapp.network.retrofit.IWikiService;

public class NetworkCreatorImpl
        implements INetworkCreator {

    private ApiFactory apiFactory;

    @Inject
    public NetworkCreatorImpl(ApiFactory apiFactory) {
        this.apiFactory = apiFactory;
    }

    @Override
    //Creating API request and returning API response / RxJava v.1
    public Observable<WholeModel> provideTranslationObserver(
            Map<String, String> params, final String API_ENDPOINT_URL) {
        ITranslateService service = (ITranslateService) apiFactory.getService(API_ENDPOINT_URL);
        return service.translateService(params);

    }

    @Override
    //Creating API request and returning API response
    public Observable<FlickrPicturesModel> providePictureObserver(
            Map<String, String> params, final String API_ENDPOINT_URL) {
//        return apiFactory.getPictureService(API_ENDPOINT_URL).pictureService(params);
        IPictureService service = (IPictureService) apiFactory.getService(API_ENDPOINT_URL);
        return service.pictureService(params);
    }

    @Override
    public Observable<ResponseBody> provideWikiObserver(
            Map<String, String> params, String API_ENDPOINT_URL) {
//        Log.i("", "");
        IWikiService service = (IWikiService) apiFactory.getService(API_ENDPOINT_URL);
        return service.wikiService(params);
    }

}