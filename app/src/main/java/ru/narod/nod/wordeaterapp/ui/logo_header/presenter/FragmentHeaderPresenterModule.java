package ru.narod.nod.wordeaterapp.ui.logo_header.presenter;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import ru.narod.nod.wordeaterapp.inject.PerFragment;
import ru.narod.nod.wordeaterapp.model.WholeModel;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.FragmentTranslationPresenterImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationListener;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.presenter.IFragmentTranslationPresenter;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;

/**
 * Provides presenter dependencies.
 */
@Module
public abstract class FragmentHeaderPresenterModule {

    @Binds
    @PerFragment
    abstract IFragmentHeaderPresenter iFragmentHeaderPresenter (
            FragmentHeaderPresenterImpl fragmentHeaderPresenter);

}