package ru.narod.nod.wordeaterapp.ui.general.view;

/**
 * The type of all views.
 * <p>
 * Note that this is not named "View" because it collides with Android's {@link android.view.View}.
 */
public interface MVPView {
}