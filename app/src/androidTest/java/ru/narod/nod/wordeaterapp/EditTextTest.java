package ru.narod.nod.wordeaterapp;

import android.support.test.rule.ActivityTestRule;
import android.widget.RelativeLayout;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import ru.narod.nod.wordeaterapp.ui.edit_text.view.FragmentEditTextImpl;
import ru.narod.nod.wordeaterapp.ui.fragment_translation.view.FragmentTranslationImpl;
import ru.narod.nod.wordeaterapp.ui.main_activity.MainActivity;

import static org.junit.Assert.assertNotNull;

/**
 * Created by User on 17.02.2018.
 */

public class EditTextTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    private MainActivity mMainActivity;
    private FragmentTranslationImpl mFragmentTranslationImpl;
    private FragmentEditTextImpl mFragmentEditTextImpl;

    private RelativeLayout fragm_container;

    @Before
    public void setUp() throws Exception {
        mMainActivity = mActivityTestRule.getActivity();
        mFragmentTranslationImpl = new FragmentTranslationImpl();
        mFragmentEditTextImpl = new FragmentEditTextImpl();
    }

    @Test
    public void getEditText() throws Exception {
        fragm_container = mMainActivity.findViewById(R.id.fragm_container);
        assertNotNull(fragm_container);
    }

    @Test
    public void setEditText_string() throws Exception {
    }

    @Test
    public void setHint() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
        mMainActivity = null;
        mActivityTestRule = null;
    }

}
